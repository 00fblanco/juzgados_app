<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;

class Usuarios extends Model implements AuthenticatableContract, AuthorizableContract
{
    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'UsuarioNombreUsuario', 'UsuarioContrasenia',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'UsuarioContrasenia'
    ];

    public function Rol(){
        return $this->belongsTo('App\Roles', 'RolId', 'RolId');
    }
    

    public function Departamento(){
        return $this->belongsTo('App\Departamentos', 'DepartamentoId', 'DepartamentoId');
    }
}
