<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;

class DelitosCausaPenal extends Model 
{

    protected $table = 'DelitosCausaPenal';

   
    public function CausaPenal(){
        return $this->belongsTo('App\CausasPenales', 'CausaPenalId', 'CausaPenalId');
    }
    
    public function Inculpado(){
        return $this->belongsTo('App\Actores', 'ActorInculpadoId', 'ActorId');
    }
    public function Agraviado(){
        return $this->belongsTo('App\Actores', 'ActorAgraviadoId', 'ActorId');
    }
}
