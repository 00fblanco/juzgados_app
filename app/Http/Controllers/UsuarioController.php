<?php

namespace App\Http\Controllers;

use App\Usuarios;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Log;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class UsuarioController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function update(Request $request){
        
        $this->validate($request, [
            'UsuarioIdd' => 'required',
            'UsuarioNick' => 'required',
            'UsuarioNombre' => 'required',
            'UsuarioApPaterno' => 'required',
            'UsuarioApMaterno' => 'required',   
            'DepartamentoId' => 'required',
            'UsuarioEstatus' => 'required',
            'RolId' => 'required'         
        ]);
        
        $data = $request->only('UsuarioContrasenia','UsuarioNick','UsuarioEstatus', 'UsuarioNombre', 'UsuarioApPaterno','UsuarioApMaterno', 
            'DepartamentoId', 'RolId');
        //error_log("->".$data["UsuarioContrasenia"]);
        //unset($data['UsuarioContrasenia']);
        if(empty($data["UsuarioContrasenia"])){
            unset($data['UsuarioContrasenia']);
        }else{
            $data["UsuarioContrasenia"] = Hash::make($data["UsuarioContrasenia"]);
        }
        

        $affected = DB::table('Usuarios')
            ->where('UsuarioId', $request->input("UsuarioIdd"))
            ->update($data);
        return response()->json(['status' => 'success', 'msg' => $affected]);
    }
    public function getById(Request $request, $id)
    {
        $usuario = Usuarios::with(['Rol','Departamento'])->where('UsuarioId', $id)->orderBy('UsuarioEstatus', 'asc')->get()->first();
        return response()->json($usuario);
    }
    public function listAll(Request $request)
    {
        $usuarios = Usuarios::with(['Rol','Departamento'])->orderBy('UsuarioEstatus', 'asc')->get();
        return response()->json($usuarios);
    }
    public function setEstatus(Request $request){
        $this->validate($request, [
            '_UsuarioId' => 'required',
            'UsuarioEstatus' => 'required']);

        $Id = $request->input('_UsuarioId');
        $UsuarioEstatus = $request->input('UsuarioEstatus');
        error_log("===>");
        error_log($Id);
        $affected = DB::table('Usuarios')
              ->where('UsuarioId', $Id)
              ->update(['UsuarioEstatus' => $UsuarioEstatus]);
        return response()->json(['status' => 'success', 'msg' => $affected]);
    }
    //
    public function store(Request $request)
    {
        $this->validate($request, [
            'UsuarioNick' => 'required',
            'UsuarioContrasenia' => 'required',
            'UsuarioNombre' => 'required',
            'UsuarioApPaterno' => 'required',
            'UsuarioApMaterno' => 'required',   
            'DepartamentoId' => 'required',
            'RolId' => 'required'         
        ]);
        
        $data = $request->only('UsuarioNick', 'UsuarioContrasenia', 'UsuarioNombre', 'UsuarioApPaterno','UsuarioApMaterno', 
            'DepartamentoId', 'RolId');
        $data["UsuarioContrasenia"] = Hash::make($data["UsuarioContrasenia"]);
        $data["UsuarioEstatus"] = 'Activo';
        $data["UsuarioApiKey"] = '';
        $result = DB::table('Usuarios')->insertGetId(
            $data
        );
        $reg["UsuarioId"] = $result;
        $reg["Accion"] = "Registro de usuario";
        $reg["Descripcion"] = "Registro de usuario con exito";
       
        DB::table('Bitacora')->insert($reg);

        if($result){
            return response()->json(['status' => 'success', 'msg' => $result]);
        }else{
            return response()->json(['status' => 'fail', 'msg' => $result]);
        }
    }
    public function authenticate(Request $request)
    {
        $this->validate($request, [
            'UsuarioNick' => 'required',
            'UsuarioContrasenia' => 'required'
        ]);

        $usuario = Usuarios::where([
                ['UsuarioNick','=' ,$request->input('UsuarioNick')],
                ['UsuarioEstatus','=', 'Activo']
            ]
            )->first();
        Log::info('Showing user: '.$usuario);

        if(empty($usuario)) return response()->json(['status' => 'fail', 'message' => 'No se encontro el usuario'],401);

        if(Hash::check($request->input('UsuarioContrasenia'), $usuario->UsuarioContrasenia)){
            $apikey = base64_encode(Str::random(40));
            Usuarios::where('UsuarioNick', $request->input('UsuarioNick'))->update(['UsuarioApiKey' => "$apikey"]);
            $reg["UsuarioId"] = $usuario->UsuarioId;
            $reg["Accion"] = "Inicio de sessión";
            $reg["Descripcion"] = "Inicio de sessión con exito";
            DB::table('Bitacora')->insert($reg);

            return response()->json(['status' => 'success','UsuarioApiKey' => $apikey, 'UsuarioRol' => $usuario->Rol]);
        }else{
            return response()->json(['status' => 'fail'],401);
        }
    }
}
