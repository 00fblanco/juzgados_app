<?php

namespace App\Http\Controllers;

use App\Departamentos;
use App\Usuarios;
use App\CausasPenales;
use App\DelitosCausaPenal;
use App\Actores;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

use Auth;
class CausasPenalesController extends Controller
{
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
    }

    public function update(Request $request){
        $reg["UsuarioId"] = Auth::user()->UsuarioId;
        $reg["Accion"] = "Actualiza causa penal ".$request->input("CausaPenalId");
        $reg["Descripcion"] = "Actualiza causa penal, CausaPenalId: ".$request->input("CausaPenalId");
        DB::table('Bitacora')->insert($reg);

        $this->validate($request, [
            'CausaPenalId' => 'required',
            'CausaPenalAveriguacionPrevia' => 'required',
            'CausaPenalPedimentoPenal' => 'required',
            'CausaPenalDescripcion' => 'required',
            'CausaPenalDetenido' => 'required',   
            'JuzgadoId' => 'required'       
        ]);
        
        $data = $request->only('CausaPenalAveriguacionPrevia','CausaPenalPedimientoPenal', 'CausaPenalPedimentoPenal', 'CausaPenalDescripcion'
        ,'CausaPenalDetenido',  'JuzgadoId');
        //error_log("->".$data["UsuarioContrasenia"]);
        //unset($data['UsuarioContrasenia']);
        $data['updated_at'] = Carbon::now();
        $affected = DB::table('causaspenales')
            ->where('CausaPenalId', $request->input("CausaPenalId"))
            ->update($data);
        return response()->json(['status' => 'success', 'msg' => $affected]);
    }
    public function updateDelito(Request $request){
        $reg["UsuarioId"] = Auth::user()->UsuarioId;
        $reg["Accion"] = "Actualiza delito ";
        $reg["Descripcion"] = "Actualiza delito, del DelitoCausaPenalId: ".$request->input("DelitoCausaPenalId");
        DB::table('Bitacora')->insert($reg);

        $this->validate($request, [
            'DelitoCausaPenalId' => 'required',
            //'incidentes' => 'required',
            //'motivo' => 'required',
            //'fechaStcJuridica' => 'required',
            //'SituacionJuridicaId' => 'required',   
            //'TipoOrdenId' => 'required'       
        ]);
        
        $data = $request->only('incidentes','motivo', 'fechaStcJuridica', 'SituacionJuridicaId'
        ,'TipoOrdenId');
        error_log("->".$request->input("DelitoCausaPenalId"));
        //unset($data['UsuarioContrasenia']);
        $data['updated_at'] = Carbon::now();
        $affected = DB::table('DelitosCausaPenal')
            ->where('DelitoCausaPenalId', $request->input("DelitoCausaPenalId"))
            ->update($data);
        return response()->json(['status' => 'success', 'msg' => $affected]);
    }
    public function updateSentencia(Request $request){
        $reg["UsuarioId"] = Auth::user()->UsuarioId;
        $reg["Accion"] = "Actualiza sentencia ";
        $reg["Descripcion"] = "Actualiza sentencia, del DelitoCausaPenalId".$request->input("DelitoCausaPenalId");
        DB::table('Bitacora')->insert($reg);


        $this->validate($request, [
            'DelitoCausaPenalId' => 'required',
            /* 'CatSentenciaId' => 'required',
            'SentenciaFecha'=> 'required',
            'SentenciaPenalidad'=> 'required',
            'SentenciaReparacionDanio'=> 'required',
            'SentenciaInhabilitacion'=> 'required',
            'SentenciaTmpInhabilitacion'=> 'required',    */
        ]);
        
        $data = $request->only('CatSentenciaId','SentenciaFecha', 'SentenciaPenalidad', 'SentenciaReparacionDanio'
        ,'SentenciaInhabilitacion', 'SentenciaTmpInhabilitacion');
        //unset($data['UsuarioContrasenia']);
        $data['updated_at'] = Carbon::now();

        $SentenciaId = DB::table('DelitosCausaPenal')
        ->select('SentenciaId')->where('DelitoCausaPenalId',  $request->input("DelitoCausaPenalId"))
        ->get()
        ->first()->SentenciaId;

        $affected = DB::table('Sentencias')
            ->where('SentenciaId', $SentenciaId)
            ->update
            (
                $data
            );
        return response()->json(['status' => 'success', 'msg' => $affected]);
    }
    public function addSentencia(Request $request){
        
        $reg["UsuarioId"] = Auth::user()->UsuarioId;
        $reg["Accion"] = "Agrega sentencia ";
        $reg["Descripcion"] = "Agregar sentencia, del DelitoCausaPenalId".$request->input("DelitoCausaPenalId");
        DB::table('Bitacora')->insert($reg);

        $this->validate($request, [
            'DelitoCausaPenalId' => 'required',
            /* 'CatSentenciaId' => 'required',
            'SentenciaFecha'=> 'required',
            'SentenciaPenalidad'=> 'required',
            'SentenciaReparacionDanio'=> 'required',
            'SentenciaInhabilitacion'=> 'required',
            'SentenciaTmpInhabilitacion'=> 'required',    */
        ]);
        
        $data = $request->only('CatSentenciaId','SentenciaFecha', 'SentenciaPenalidad', 'SentenciaReparacionDanio'
        ,'SentenciaInhabilitacion', 'SentenciaTmpInhabilitacion');
        //unset($data['UsuarioContrasenia']);
        $data['created_at'] = Carbon::now();
        $data['updated_at'] = Carbon::now();

        $SentenciaId = DB::table('Sentencias')->insertGetId(
            $data
        );
        
        if(!$SentenciaId){
            return response()->json(['status' => 'fail', 'msg' => 'No se pudo agregar la sentencia, verificar con el admin']);
        }

        $affected = DB::table('DelitosCausaPenal')
            ->where('DelitoCausaPenalId', $request->input("DelitoCausaPenalId"))
            ->update
            (
                ['SentenciaId' => $SentenciaId, 'updated_at' => Carbon::now()]
            );
        return response()->json(['status' => 'success', 'msg' => $affected]);
    }
    public function updateRecurso(Request $request){
        $reg["UsuarioId"] = Auth::user()->UsuarioId;
        $reg["Accion"] = "Actualiza recurso  ";
        $reg["Descripcion"] = "Actualiza recurso, del DelitoCausaPenalId ".$request->input("DelitoCausaPenalId");
        DB::table('Bitacora')->insert($reg);

        $this->validate($request, [
            'DelitoCausaPenalId' => 'required',
            /* 'RecursoOfrecimientoPruebas' => 'required',
            'RecursoCierreAudiencia'=> 'required',
            'RecursoApelacion'=> 'required',
            'RecursoExtensionPenal'=> 'required',
            'RecursoSobreseimiento'=> 'required',
            'RecursoObservacion'=> 'required',    */
        ]);
        
        $data = $request->only('RecursoOfrecimientoPruebas','RecursoCierreAudiencia', 'RecursoApelacion', 'RecursoExtensionPenal'
        ,'RecursoSobreseimiento', 'RecursoObservacion');
        //unset($data['UsuarioContrasenia']);
        $data['updated_at'] = Carbon::now();

        $RecursoId = DB::table('DelitosCausaPenal')
        ->select('RecursoId')->where('DelitoCausaPenalId',  $request->input("DelitoCausaPenalId"))
        ->get()
        ->first()->RecursoId;
        

        $affected = DB::table('Recursos')
            ->where('RecursoId', $RecursoId)
            ->update
            (
                $data
            );
        return response()->json(['status' => 'success', 'msg' => $affected]);
    }
    public function addRecurso(Request $request){
        $reg["UsuarioId"] = Auth::user()->UsuarioId;
        $reg["Accion"] = "Agregar recurso  ";
        $reg["Descripcion"] = "Agrega recurso, del DelitoCausaPenalId: ".$request->input("DelitoCausaPenalId");
        DB::table('Bitacora')->insert($reg);


        $this->validate($request, [
            'DelitoCausaPenalId' => 'required',
           /*  'RecursoOfrecimientoPruebas' => 'required',
            'RecursoCierreAudiencia'=> 'required',
            'RecursoApelacion'=> 'required',
            'RecursoExtensionPenal'=> 'required',
            'RecursoSobreseimiento'=> 'required',
            'RecursoObservacion'=> 'required',    */
        ]);
        
        $data = $request->only('RecursoOfrecimientoPruebas','RecursoCierreAudiencia', 'RecursoApelacion', 'RecursoExtensionPenal'
        ,'RecursoSobreseimiento', 'RecursoObservacion');
        //unset($data['UsuarioContrasenia']);
        $data['created_at'] = Carbon::now();
        $data['updated_at'] = Carbon::now();

        $RecursoId = DB::table('Recursos')->insertGetId(
            $data
        );
        
        if(!$RecursoId){
            return response()->json(['status' => 'fail', 'msg' => 'No se pudo agregar el recurso, verificar con el admin']);
        }

        $affected = DB::table('DelitosCausaPenal')
            ->where('DelitoCausaPenalId', $request->input("DelitoCausaPenalId"))
            ->update
            (
                ['RecursoId' => $RecursoId, 'updated_at' => Carbon::now()]
            );
        return response()->json(['status' => 'success', 'msg' => $affected]);
    }

    public function addOda(Request $request){
        
        $reg["UsuarioId"] = Auth::user()->UsuarioId;
        $reg["Accion"] = "Agregar Oda  ";
        $reg["Descripcion"] = "Agrega Oda del DelitoCausaPenalId: ".$request->input("DelitoCausaPenalId");
        DB::table('Bitacora')->insert($reg);

        $this->validate($request, [
            'DelitoCausaPenalId' => 'required',

           /*  'TipoOrdenId'=> 'required',
            'OdaFechaOrdenLibrada'=> 'required',
            'OdaNumOfcOrdenLibrada'=> 'required',
            'OdaFechaTranscripcionOrden'=> 'required',
            'OdaNumOfcTranscripcionOrden'=> 'required',

            'LugarCalle'=> 'required',
            'LugarNumInt'=> 'required',
            'LugarNumExt'=> 'required',
            'LugarLatitud'=> 'required',
            'LugarLongitud'=> 'required',
            'LugarTramoCarretero'=> 'required',
            'LugarNombreAgente'=> 'required',
            'LugarDescripcionCaptura'=> 'required',   */ 
        ]);
        
        $Lugar = $request->only('LugarCalle','LugarNumInt', 'LugarNumExt', 'LugarLatitud', 'LugarLongitud'
        ,'LugarTramoCarretero', 'LugarNombreAgente', 'LugarDescripcionCaptura');
        $Oda = $request->only('TipoOrdenId', 'OdaFechaOrdenLibrada', 'OdaNumOfcOrdenLibrada', 'OdaFechaTranscripcionOrden',
            'OdaNumOfcTranscripcionOrden'
        );
        //unset($data['UsuarioContrasenia']);
        $Lugar['created_at'] = Carbon::now();
        $Lugar['updated_at'] = Carbon::now();
        $Oda['created_at'] = Carbon::now();
        $Oda['updated_at'] = Carbon::now();


        $LugarId = DB::table('LugaresAprehension')->insertGetId(
            $Lugar
        );
        
        if(!$LugarId){
            return response()->json(['status' => 'fail', 'msg' => 'No se pudo agregar el Lugar, verificar con el admin']);
        }

        $Oda['LugarAprehensionId'] = $LugarId;
        $OdaId = DB::table('Oda')->insertGetId(
            $Oda
        );
        
        if(!$OdaId){
            return response()->json(['status' => 'fail', 'msg' => 'No se pudo agregar la Oda, verificar con el admin']);
        }

        $affected = DB::table('DelitosCausaPenal')
            ->where('DelitoCausaPenalId', $request->input("DelitoCausaPenalId"))
            ->update
            (
                ['OdaId' => $OdaId, 'updated_at' => Carbon::now()]
            );
        return response()->json(['status' => 'success', 'msg' => $affected]);
    }
    public function updateOda(Request $request){
        $reg["UsuarioId"] = Auth::user()->UsuarioId;
        $reg["Accion"] = "Actualiza Oda  ";
        $reg["Descripcion"] = "Actualiza Oda de la CausaPenalId: ".$request->input("DelitoCausaPenalId");
        DB::table('Bitacora')->insert($reg);

        $this->validate($request, [
            'DelitoCausaPenalId' => 'required',
/* 
            'TipoOrdenId'=> 'required',
            'OdaFechaOrdenLibrada'=> 'required',
            'OdaNumOfcOrdenLibrada'=> 'required',
            'OdaFechaTranscripcionOrden'=> 'required',
            'OdaNumOfcTranscripcionOrden'=> 'required',

            'LugarCalle'=> 'required',
            'LugarNumInt'=> 'required',
            'LugarNumExt'=> 'required',
            'LugarLatitud'=> 'required',
            'LugarLongitud'=> 'required',
            'LugarTramoCarretero'=> 'required',
            'LugarNombreAgente'=> 'required',
            'LugarDescripcionCaptura'=> 'required',    */
        ]);
        
        $Lugar = $request->only('LugarCalle','LugarNumInt', 'LugarNumExt', 'LugarLatitud', 'LugarLongitud'
        ,'LugarTramoCarretero', 'LugarNombreAgente', 'LugarDescripcionCaptura');
        $Oda = $request->only('TipoOrdenId', 'OdaFechaOrdenLibrada', 'OdaNumOfcOrdenLibrada', 'OdaFechaTranscripcionOrden',
            'OdaNumOfcTranscripcionOrden'
        );

        $OdaReg = DB::table('DelitosCausaPenal')
            ->leftJoin('Oda', 'Oda.OdaId', '=', 'DelitosCausaPenal.OdaId')
        ->select('Oda.OdaId','Oda.LugarAprehensionId')->where('DelitosCausaPenal.DelitoCausaPenalId',  $request->input("DelitoCausaPenalId"))
        ->get()
        ->first();

        //unset($data['UsuarioContrasenia']);
        $Lugar['updated_at'] = Carbon::now();
        $Oda['updated_at'] = Carbon::now();


        $affected = DB::table('Oda')
            ->where('OdaId', $OdaReg->OdaId)
            ->update
            (
                $Oda
            );

        $affected = DB::table('LugaresAprehension')
            ->where('LugarAprehensionId', $OdaReg->LugarAprehensionId)
            ->update
            (
                $Lugar
            );
        return response()->json(['status' => 'success', 'msg' => $affected]);
    }

    public function setEstatus(Request $request){
        $reg["UsuarioId"] = Auth::user()->UsuarioId;
        $reg["Accion"] = "Actualiza estatus causa penal  ";
        $reg["Descripcion"] = "Actualiza estatus causa penal, CausaPenalId: ".$request->input("CausaPenalId");
        DB::table('Bitacora')->insert($reg);

        $this->validate($request, [
            'CausaPenalId' => 'required',
            'CausaPenalEstatus' => 'required']);

        $CausaPenalId = $request->input('CausaPenalId');
        $CausaPenalEstatus = $request->input('CausaPenalEstatus');
        $affected = DB::table('causaspenales')
              ->where('CausaPenalId', $CausaPenalId)
              ->update(['CausaPenalEstatus' => $CausaPenalEstatus]);
        return response()->json(['status' => 'success', 'msg' => $affected]);
    }

    public function getById(Request $request, $id)
    {


        $causa = DB::table('CausasPenales')->
            where("CausasPenales.CausaPenalId", $id)->get()->first();
        return response()->json($causa);
    }
    public function getCausasPenales(Request $request){
        $reg["UsuarioId"] = Auth::user()->UsuarioId;
        $reg["Accion"] = "Consulta las causas penales";
        $reg["Descripcion"] = "Consulta las causas penales";
        DB::table('Bitacora')->insert($reg);

        $causas = CausasPenales::with(['Delitos', 'Delitos.Inculpado','Delitos.Agraviado'])->get();

        return response()->json(['causas_penales' => $causas]);
    }
    public function getCausasPenalesR1(Request $request, $year, $month){
        $reg["UsuarioId"] = Auth::user()->UsuarioId;
        $reg["Accion"] = "Consulta las causas penales para reporte 1";
        $reg["Descripcion"] = "Consulta las causas penales para reporte 1";
        DB::table('Bitacora')->insert($reg);

        $causas = DB::table('DelitosCausaPenal')
        ->join('Actores as ActorInculpado', 'ActorInculpado.ActorId', '=', 'DelitosCausaPenal.ActorInculpadoId')
        ->join('CatDelitos', 'CatDelitos.DelitoId', '=', 'DelitosCausaPenal.DelitoId')
        ->leftjoin('Sentencias', 'Sentencias.SentenciaId','=', 'DelitosCausaPenal.SentenciaId')
        ->leftjoin('Recursos', 'Recursos.RecursoId','=', 'DelitosCausaPenal.RecursoId')
        ->leftJoin('Oda', 'Oda.OdaId', '=', 'DelitosCausaPenal.OdaId')
        ->leftJoin('CatTipoOrden as OdaCatTipoOrden', 'OdaCatTipoOrden.TipoOrdenId', '=', 'Oda.TipoOrdenId')
        ->leftJoin('CatTipoOrden as DelitoCatTipoOrden', 'DelitoCatTipoOrden.TipoOrdenId', '=', 'DelitosCausaPenal.TipoOrdenId')
        ->leftJoin('LugaresAprehension', 'LugaresAprehension.LugarAprehensionId', '=', 'Oda.LugarAprehensionId')
        ->leftjoin('CausasPenales', 'CausasPenales.CausaPenalId','=', 'DelitosCausaPenal.CausaPenalId')
        ->Join('ActoresAgraviados', 'ActoresAgraviados.DelitoCausaPenalId', '=', 'DelitosCausaPenal.DelitoCausaPenalId')
        ->leftjoin('Actores as ActorAgraviado', 'ActorAgraviado.ActorId','=', 'ActoresAgraviados.ActorAgraviadoId')
        ->select('CausasPenales.*','OdaCatTipoOrden.TipoOrdenDescripcion as OdaTipoOrdenDescripcion','DelitoCatTipoOrden.TipoOrdenDescripcion as DelitoTipoOrdenDescripcion','DelitosCausaPenal.*','LugaresAprehension.*', 'Oda.*','Sentencias.*','Recursos.*', 'CatDelitos.*',
        'ActorInculpado.ActorId as InculpadoId','ActorInculpado.ActorNombre as InculpadoNombre', 'ActorInculpado.ActorApPaterno as InculpadoApPaterno','ActorInculpado.ActorApMaterno as InculpadoApMaterno',
        'ActorInculpado.ActorSexo as InculpadoSexo', 'ActorInculpado.ActorFechaNacimiento as InculpadoFechaNacimiento', 
        'ActorInculpado.ActorCalle as InculpadoCalle', 'ActorInculpado.ActorNumExterior as InculpadoNumExterior',
        'ActorInculpado.ActorNumInterior as InculpadoNumInterior', 'ActorInculpado.ActorLatitud as InculpadoLatitud',
        'ActorInculpado.ActorLongitud as InculpadoLongitud',

        'ActorAgraviado.ActorId as AgraviadoId','ActorAgraviado.ActorNombre as AgraviadoNombre', 'ActorAgraviado.ActorApPaterno as AgraviadoApPaterno','ActorAgraviado.ActorApMaterno as AgraviadoApMaterno',
        'ActorAgraviado.ActorSexo as AgraviadoSexo', 'ActorAgraviado.ActorFechaNacimiento as AgraviadoFechaNacimiento', 
        'ActorAgraviado.ActorCalle as AgraviadoCalle', 'ActorAgraviado.ActorNumExterior as AgraviadoNumExterior',
        'ActorAgraviado.ActorNumInterior as AgraviadoNumInterior', 'ActorAgraviado.ActorLatitud as AgraviadoLatitud',
        'ActorAgraviado.ActorLongitud as AgraviadoLongitud',
         )
         ->whereYear('CausasPenales.updated_at', $year)
         ->whereMonth('CausasPenales.updated_at', $month)
        
        ->get();
        return response()->json(['rows' => $causas]);
    }
    public function store(Request $request)
    {
        

        /*error_log($request->header());
        $key = explode(' ', $request->headers('Authorization'));
        error_log($key);
        $user = Usuarios::where('UsuarioApiKey', $key[1])->first();
        if(!empty($user)){
            return response()->json(['status' => 'fail', 'msg' => 'Usuario no valido']);
        }*/
        //error_log($request["UsuarioId"]);
        //error_log('AQUI MERO');
        //error_log(Auth::user()->UsuarioId);
        $this->validate($request, [
            'CausaPenalAveriguacionPrevia' => 'required',
            'CausaPenalPedimentoPenal' => 'required',
            'CausaPenalDescripcion' => 'required',
            'CausaPenalDetenido' => 'required',
            'JuzgadoId' => 'required',      
        ]);
        
        $data = $request->only('CausaPenalAveriguacionPrevia', 'CausaPenalPedimentoPenal',
         'CausaPenalDescripcion', 'CausaPenalDetenido','JuzgadoId');

        $anio = date("Y");

        $ultFolio = DB::table('CausasPenales')
            ->where('CausaPenalAnio', $anio)
            ->max('CausaPenalFolio');
        
        if(empty($ultFolio)){
            $ultFolio = 1;
        }else{
            $ultFolio = $ultFolio + 1;
        }
        $data["UsuarioId"] = Auth::user()->UsuarioId;
        $data["CausaPenalAnio"] = $anio;
        $data["CausaPenalFolio"] = $ultFolio;
        $data["CausaPenalEstatus"] = 'VIGENTE';
        $data["CausaPenalIdGenerado"] = "$ultFolio$anio";
        $data['created_at'] = Carbon::now();
        $result = DB::table('CausasPenales')->insertGetId(
            $data
        );
        $reg["UsuarioId"] = Auth::user()->UsuarioId;
        $reg["Accion"] = "Agregar causa penal";
        $reg["Descripcion"] = "Agregar causa penal, IdGenerado: $ultFolio$anio";
        DB::table('Bitacora')->insert($reg);
        if($result){
            return response()->json(['status' => 'success', 'CausaPenalId' => $result]);
        }else{
            return response()->json(['status' => 'fail', 'msg' => $result]);
        }
    }
    public function updateActor(Request $request)
    {
        $reg["UsuarioId"] = Auth::user()->UsuarioId;
        $reg["Accion"] = "Actualiza actor";
        $reg["Descripcion"] = "Actualiza actor";
        DB::table('Bitacora')->insert($reg);

        $this->validate($request, [
            'ActorId' => 'required',
            'ActorNombre'=> 'required',
            'ActorApPaterno'=> 'required',
            'ActorApMaterno'=>'required',
            'ActorSexo'=> 'required',
            'ActorFechaNacimiento'=> 'required',
        ]);
        
        $actor = $request->only(
         'ActorDetenido', 'ActorNombre','ActorApPaterno','ActorApMaterno', 'ActorSexo', 'ActorFechaNacimiento',
            'ActorCalle', 'ActorNumExterior', 'ActorNumInterior', 'ActorLatitud', 'ActorLongitud'
        );

        $actor['updated_at'] = Carbon::now();
        $result = DB::table('Actores')->where(
            'ActorId', $request->ActorId
        )->update($actor);

        if($result){
            return response()->json(['status' => 'success', 'msg' => $result]);
        }else{
            return response()->json(['status' => 'fail', 'msg' => $result]);
        }
    }
    public function addActor(Request $request)
    {
        $reg["UsuarioId"] = Auth::user()->UsuarioId;
        $reg["Accion"] = "Agregar actor";
        $reg["Descripcion"] = "Agrega actor";
        DB::table('Bitacora')->insert($reg);


        $this->validate($request, [
            'CausaPenalId'=> 'required',
            'ActorNombre'=> 'required',
            'ActorApPaterno'=> 'required',
            'ActorApMaterno'=>'required',
            'ActorSexo'=> 'required',
            'ActorFechaNacimiento'=> 'required',
        ]);
        
        $actor = $request->only(
         'ActorDetenido', 'ActorNombre','ActorApPaterno','ActorApMaterno', 'ActorSexo', 'ActorFechaNacimiento',
            'ActorCalle', 'ActorNumExterior', 'ActorNumInterior', 'ActorLatitud', 'ActorLongitud'
        );
        $delitos = $request->only('delitos')["delitos"];
        $CausaPenalId = $request->only('CausaPenalId')["CausaPenalId"];


        $actor['created_at'] = Carbon::now();
        $actor['updated_at'] = Carbon::now();
        $ActorId = DB::table('Actores')->insertGetId(
            $actor
        );
        
        if(!$ActorId){
            return response()->json(['status' => 'fail', 'msg' => 'No se pudo agregar el actor, verificar con el admin']);
        }
        
        $CausaPenalIdGenerado = DB::table('CausasPenales')
            ->select('CausaPenalIdGenerado')->where('CausaPenalId', $CausaPenalId)
            ->get()
            ->first()->CausaPenalIdGenerado;
        foreach ($delitos as &$delitoId) {
            $result = DB::table('DelitosCausaPenal')->insert(
                ['CausaPenalIdGenerado' => $CausaPenalIdGenerado,'CausaPenalId' => $CausaPenalId, 'DelitoId' => $delitoId, 'ActorInculpadoId' => $ActorId, 'updated_at' => Carbon::now(), 'created_at' => Carbon::now()]
            );
        }

        if($result){
            return response()->json(['status' => 'success', 'msg' => $result]);
        }else{
            return response()->json(['status' => 'fail', 'msg' => $result]);
        }
    }
    public function addAgraviado(Request $request)
    {
        $reg["UsuarioId"] = Auth::user()->UsuarioId;
        $reg["Accion"] = "Agregar agraviado";
        $reg["Descripcion"] = "Agrega agraviado";
        DB::table('Bitacora')->insert($reg);


        $this->validate($request, [
            'DelitoCausaPenalId'=> 'required',
            'ActorNombre'=> 'required',
            'ActorApPaterno'=> 'required',
            'ActorApMaterno'=>'required',
            'ActorSexo'=> 'required',
            'ActorFechaNacimiento'=> 'required',
        ]);

        
        
        $actor = $request->only(
             'ActorNombre','ActorApPaterno','ActorApMaterno', 'ActorSexo', 'ActorFechaNacimiento',
            'ActorCalle', 'ActorNumExterior', 'ActorNumInterior', 'ActorLatitud', 'ActorLongitud'
        );
        $DelitoCausaPenalId = $request->only('DelitoCausaPenalId')["DelitoCausaPenalId"];
        $actor['ActorDetenido'] = 'NO';
        $actor['created_at'] = Carbon::now();
        $actor['updated_at'] = Carbon::now();
        $ActorId = DB::table('Actores')->insertGetId(
            $actor
        );
        
        if(!$ActorId){
            return response()->json(['status' => 'fail', 'msg' => 'No se pudo agregar el actor, verificar con el admin']);
        }
        
        /* $CausaPenalId = DB::table('DelitosCausaPenal')->select('CausaPenalId')
        ->where('DelitoCausaPenalId', $DelitoCausaPenalId)
        ->first()->CausaPenalId; */

        $result = DB::table('ActoresAgraviados')
        //->where('DelitoCausaPenalId', $DelitoCausaPenalId)
        ->insert
        (
            ['ActorAgraviadoId' => $ActorId, 'DelitoCausaPenalId' => $DelitoCausaPenalId]
        );
        

        if($result){
            return response()->json(['status' => 'success', 'msg' => $result]);
        }else{
            return response()->json(['status' => 'fail', 'msg' => $result]);
        }
    }

    public function getDelitosDeCausaPenal(Request $request){
        $reg["UsuarioId"] = Auth::user()->UsuarioId;
        $reg["Accion"] = "Consulta delitos de la causa penal";
        $reg["Descripcion"] = "Consulta delitos de la causa penal";
        DB::table('Bitacora')->insert($reg);

        $CausaPenalId = $request->id;
        $rows = DB::table('DelitosCausaPenal')
            ->join('Actores as ActorInculpado', 'ActorInculpado.ActorId', '=', 'DelitosCausaPenal.ActorInculpadoId')
            ->join('CatDelitos', 'CatDelitos.DelitoId', '=', 'DelitosCausaPenal.DelitoId')
            ->leftjoin('Sentencias', 'Sentencias.SentenciaId','=', 'DelitosCausaPenal.SentenciaId')
            ->leftjoin('Recursos', 'Recursos.RecursoId','=', 'DelitosCausaPenal.RecursoId')
            ->leftJoin('Oda', 'Oda.OdaId', '=', 'DelitosCausaPenal.OdaId')
            ->leftJoin('LugaresAprehension', 'LugaresAprehension.LugarAprehensionId', '=', 'Oda.LugarAprehensionId')
            ->leftjoin('CausasPenales', 'CausasPenales.CausaPenalId','=', 'DelitosCausaPenal.CausaPenalId')
            ->leftJoin('ActoresAgraviados', 'ActoresAgraviados.DelitoCausaPenalId', '=', 'DelitosCausaPenal.DelitoCausaPenalId')
            ->leftjoin('Actores as ActorAgraviado', 'ActorAgraviado.ActorId','=', 'ActoresAgraviados.ActorAgraviadoId')
            ->select('DelitosCausaPenal.*','LugaresAprehension.*', 'Oda.*','Sentencias.*','Recursos.*', 'CatDelitos.*',
            'ActorInculpado.ActorId as InculpadoId','ActorInculpado.ActorDetenido as InculpadoDetenido','ActorInculpado.ActorNombre as InculpadoNombre', 'ActorInculpado.ActorApPaterno as InculpadoApPaterno','ActorInculpado.ActorApMaterno as InculpadoApMaterno',
            'ActorInculpado.ActorSexo as InculpadoSexo', 'ActorInculpado.ActorFechaNacimiento as InculpadoFechaNacimiento', 
            'ActorInculpado.ActorCalle as InculpadoCalle', 'ActorInculpado.ActorNumExterior as InculpadoNumExterior',
            'ActorInculpado.ActorNumInterior as InculpadoNumInterior', 'ActorInculpado.ActorLatitud as InculpadoLatitud',
            'ActorInculpado.ActorLongitud as InculpadoLongitud',

            'ActorAgraviado.ActorId as AgraviadoId','ActorAgraviado.ActorNombre as AgraviadoNombre', 'ActorAgraviado.ActorApPaterno as AgraviadoApPaterno','ActorAgraviado.ActorApMaterno as AgraviadoApMaterno',
            'ActorAgraviado.ActorSexo as AgraviadoSexo', 'ActorAgraviado.ActorFechaNacimiento as AgraviadoFechaNacimiento', 
            'ActorAgraviado.ActorCalle as AgraviadoCalle', 'ActorAgraviado.ActorNumExterior as AgraviadoNumExterior',
            'ActorAgraviado.ActorNumInterior as AgraviadoNumInterior', 'ActorAgraviado.ActorLatitud as AgraviadoLatitud',
            'ActorAgraviado.ActorLongitud as AgraviadoLongitud',

            'DelitosCausaPenal.TipoOrdenId as CPTipoOrdenId'

             )
            ->where('DelitosCausaPenal.CausaPenalId', $CausaPenalId)
            ->get();


        return response()->json(['rows' => $rows]);
    }

    public function getAgraviados(Request $request){
        $reg["UsuarioId"] = Auth::user()->UsuarioId;
        $reg["Accion"] = "Consulta agraviado de la causa penal";
        $reg["Descripcion"] = "Consulta agraviado de la causa penal";
        DB::table('Bitacora')->insert($reg);

        $DelitoCausaPenalId = $request->id;
        $rows = DB::table('ActoresAgraviados')
            ->join('Actores', 'Actores.ActorId', '=', 'ActoresAgraviados.ActorAgraviadoId')
            ->join('DelitosCausaPenal', 'DelitosCausaPenal.DelitoCausaPenalId', '=', 'ActoresAgraviados.DelitoCausaPenalId')
            ->join('CatDelitos','CatDelitos.DelitoId', '=', 'DelitosCausaPenal.DelitoId')
            ->select('ActoresAgraviados.*', 'Actores.*', 'CatDelitos.*')
            ->where('ActoresAgraviados.DelitoCausaPenalId', $DelitoCausaPenalId)
            ->get();

        
        return response()->json(['rows' => $rows]);
    }
}