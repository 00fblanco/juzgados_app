<?php

namespace App\Http\Controllers;

use App\Departamentos;
use App\Roles;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;


class CatalogosController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getDeptosRoles(Request $request){

        
        $reg["UsuarioId"] = Auth::user()->UsuarioId;
        $reg["Accion"] = "Consulta el catalogo de roles";
        $reg["Descripcion"] = "Consulta el catalogo de roles";
        DB::table('Bitacora')->insert($reg);

        $departamentos = Departamentos::all();
        $roles = Roles::all();

        return response()->json(['departamentos' => $departamentos,'roles' =>  $roles]);
    }

    public function getJuzgados(Request $request){

        $reg["UsuarioId"] = Auth::user()->UsuarioId;
        $reg["Accion"] = "Consulta el catalogo de juzgados";
        $reg["Descripcion"] = "Consulta el catalogo de juzgados";
        DB::table('Bitacora')->insert($reg);


        $juzgados = DB::table('juzgados')->get();

        return response()->json(['juzgados' => $juzgados]);
    }

    public function getTiposActor(Request $request){
        $reg["UsuarioId"] = Auth::user()->UsuarioId;
        $reg["Accion"] = "Consulta el catalogo de CatTipoActor";
        $reg["Descripcion"] = "Consulta el catalogo de CatTipoActor";
        DB::table('Bitacora')->insert($reg);


        $rows = DB::table('CatTipoActor')->get();

        return response()->json(['CatTipoActor' => $rows]);
    }
    public function getCatDelitos(Request $request){
        $reg["UsuarioId"] = Auth::user()->UsuarioId;
        $reg["Accion"] = "Consulta el catalogo de CatDelitos";
        $reg["Descripcion"] = "Consulta el catalogo de CatDelitos";
        DB::table('Bitacora')->insert($reg);

        
        $rows = DB::table('CatDelitos')->get();

        return response()->json(['CatDelitos' => $rows]);
    }
    public function getCatTipoOrden(Request $request){
        $reg["UsuarioId"] = Auth::user()->UsuarioId;
        $reg["Accion"] = "Consulta el catalogo de CatTipoOrden";
        $reg["Descripcion"] = "Consulta el catalogo de CatTipoOrden";
        DB::table('Bitacora')->insert($reg);

        $rows = DB::table('CatTipoOrden')->get();

        return response()->json(['rows' => $rows]);
    }
    public function getCatSituacionJuridica(Request $request){

        $reg["UsuarioId"] = Auth::user()->UsuarioId;
        $reg["Accion"] = "Consulta el catalogo de CatSituacionJuridica";
        $reg["Descripcion"] = "Consulta el catalogo de CatSituacionJuridica";
        DB::table('Bitacora')->insert($reg);


        $rows = DB::table('CatSituacionJuridica')->get();

        return response()->json(['rows' => $rows]);
    }
    public function getCatSentencias(Request $request){
        $reg["UsuarioId"] = Auth::user()->UsuarioId;
        $reg["Accion"] = "Consulta el catalogo de CatSentencias";
        $reg["Descripcion"] = "Consulta el catalogo de CatSentencias";
        DB::table('Bitacora')->insert($reg);


        $rows = DB::table('CatSentencias')->get();

        return response()->json(['rows' => $rows]);
    }
    
}