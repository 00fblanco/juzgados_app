<?php

namespace App\Providers;

use App\Usuarios;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\ServiceProvider;
use lluminate\Http\Request;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
        
    }

    /**
     * Boot the authentication services for the application.
     *
     * @return void
     */
    public function boot()
    {
        // Here you may define how you wish users to be authenticated for your Lumen
        // application. The callback which receives the incoming request instance
        // should return either a User instance or null. You're free to obtain
        // the User instance via an API token or any other method necessary.
        error_log("In AuthServicePRovider");
        //error_log(app('request')->header('Authorization'));
        $this->app['auth']->viaRequest('api', function ($request) {
            error_log($request->header('Authorization'));
            if ($request->header('Authorization')) {
                error_log("In via auth");
                $key = explode(' ', $request->header('Authorization'));
                $user = Usuarios::where('UsuarioApiKey', $key[1])->first();
                if(!empty($user)){
                    $request->request->add(['UsuarioId' => $user->UsuarioId]);
                }
                return $user;
            }
        });
    }
}
