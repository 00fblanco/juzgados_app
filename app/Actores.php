<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;

class Actores extends Model 
{

    protected $table = 'Actores';

   
    public function DelitosInculpado(){
        return $this->hasMany('App\DelitosCausaPenal', 'InculpadoId', 'ActorId');
    }
    
    public function DelitosAgraviado(){
        return $this->hasMany('App\DelitosCausaPenal', 'AgraviadoId', 'ActorId');
    }
}
