<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;

class CausasPenales extends Model 
{

    protected $table = 'CausasPenales';


    public function Delitos(){
        return $this->hasMany('App\DelitosCausaPenal', 'CausaPenalId', 'CausaPenalId');
    }
    
}
