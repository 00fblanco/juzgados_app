<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Sentencias extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Sentencias', function (Blueprint $table) {
            $table->bigIncrements('SentenciaId');


            $table->unsignedBigInteger('CatSentenciaId')->nullable();
            $table->foreign('CatSentenciaId')->references('SentenciaId')->on('CatSentencias');
            $table->date('SentenciaFecha')->nullable();
            $table->longText('SentenciaPenalidad')->nullable();
            $table->longText('SentenciaReparacionDanio')->nullable();
            $table->integer('SentenciaInhabilitacion')->nullable();
            $table->longText('SentenciaTmpInhabilitacion')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Sentencias');
    }
}
