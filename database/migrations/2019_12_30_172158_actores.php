<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Actores extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Actores', function (Blueprint $table) {
            $table->bigIncrements('ActorId');
            $table->string('ActorNombre', 30);
            $table->string('ActorApPaterno', 30);
            $table->string('ActorApMaterno', 30);
            $table->enum('ActorSexo', ['Masculino', 'Femenino']);
            $table->date('ActorFechaNacimiento');
            $table->enum('ActorDetenido', ['SI', 'NO', 'EN PROCESO']);
            $table->string('ActorCalle', 30);
            $table->string('ActorNumExterior', 30);
            $table->string('ActorNumInterior', 30);
            $table->string('ActorLatitud', 30);
            $table->string('ActorLongitud', 30);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Actores');
    }
}
