<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class LugarAprehencion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('LugaresAprehension', function (Blueprint $table) {
            $table->bigIncrements('LugarAprehensionId');

            $table->string('LugarCalle', 50)->nullable();
            $table->string('LugarNumInt')->nullable();
            $table->string('LugarNumExt')->nullable();
            $table->string('LugarLatitud', 50)->nullable();
            $table->string('LugarLongitud', 50)->nullable();
            $table->string('LugarTramoCarretero', 50)->nullable();
            $table->string('LugarNombreAgente', 100)->nullable();
            $table->longText('LugarDescripcionCaptura')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('LugaresAprehension');
    }
}
