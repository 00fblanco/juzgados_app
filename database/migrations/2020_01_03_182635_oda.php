<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Oda extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Oda', function (Blueprint $table) {
            $table->bigIncrements('OdaId');

            $table->unsignedBigInteger('TipoOrdenId')->nullable();
            $table->foreign('TipoOrdenId')->references('TipoOrdenId')->on('CatTipoOrden');
            
            $table->unsignedBigInteger('LugarAprehensionId')->nullable();
            $table->foreign('LugarAprehensionId')->references('LugarAprehensionId')->on('LugaresAprehension');

            $table->date('OdaFechaOrdenLibrada')->nullable();
            $table->integer('OdaNumOfcOrdenLibrada')->nullable();
            $table->date('OdaFechaTranscripcionOrden')->nullable();
            $table->integer('OdaNumOfcTranscripcionOrden')->nullable();
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Oda');
    }
}
