<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ActoresAgraviados extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ActoresAgraviados', function (Blueprint $table) {
            $table->unsignedBigInteger('DelitoCausaPenalId');
            $table->foreign('DelitoCausaPenalId')->references('DelitoCausaPenalId')->on('DelitosCausaPenal');

            $table->unsignedBigInteger('ActorAgraviadoId');
            $table->foreign('ActorAgraviadoId')->references('ActorId')->on('Actores');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('ActoresAgraviados');
    }
}
