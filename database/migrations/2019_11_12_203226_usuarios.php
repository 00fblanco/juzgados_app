<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Usuarios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Usuarios', function (Blueprint $table) {
            $table->bigIncrements('UsuarioId');
            $table->string('UsuarioNick', 50)->unique();
            $table->string('UsuarioContrasenia');
            $table->string('UsuarioApiKey', 150);
            $table->string('UsuarioNombre', 30);
            $table->string('UsuarioApPaterno', 20);
            $table->string('UsuarioApMaterno', 20);
            $table->enum('UsuarioEstatus', ['Activo', 'Inactivo']);
            
            $table->timestamps();

            // references
            //$table->unsignedBigInteger('TipoUsuarioId');
            //$table->foreign('TipoUsuarioId')->references('TipoUsuarioId')->on('TiposUsuario');

            $table->unsignedBigInteger('RolId');
            $table->foreign('RolId')->references('RolId')->on('Roles');

            $table->unsignedBigInteger('DepartamentoId');
            $table->foreign('DepartamentoId')->references('DepartamentoId')->on('Departamentos');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Departamentos');
    }
}
