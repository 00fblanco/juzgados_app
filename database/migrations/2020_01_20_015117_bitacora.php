<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Bitacora extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Bitacora', function (Blueprint $table) {
            $table->bigIncrements('BitacoraId');
            $table->string('CausaPenalIdGenerado', 20)->nullable();
            $table->string('Accion', 250);
            $table->string('Descripcion', 250);
            

            $table->unsignedBigInteger('UsuarioId');
            $table->foreign('UsuarioId')->references('UsuarioId')->on('Usuarios');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::create('Bitacora');
    }
}
