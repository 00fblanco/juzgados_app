<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class DelitosCausaPenal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('DelitosCausaPenal', function (Blueprint $table) {
            $table->bigIncrements('DelitoCausaPenalId');


            $table->unsignedBigInteger('CausaPenalId')->nullable();
            $table->foreign('CausaPenalId')->references('CausaPenalId')->on('CausasPenales');

            $table->string('CausaPenalIdGenerado', 20);
            $table->foreign('CausaPenalIdGenerado')->references('CausaPenalIdGenerado')->on('CausasPenales');

            $table->unsignedBigInteger('ActorInculpadoId');
            $table->foreign('ActorInculpadoId')->references('ActorId')->on('Actores');
            $table->unsignedBigInteger('DelitoId');
            $table->foreign('DelitoId')->references('DelitoId')->on('CatDelitos');
            // $table->unsignedBigInteger('ActorAgraviadoId')->nullable();
            // $table->foreign('ActorAgraviadoId')->references('ActorId')->on('Actores');

            $table->longText('incidentes')->nullable();
            $table->longText('motivo')->nullable();
            $table->date('fechaStcJuridica')->nullable();
            
            $table->unsignedBigInteger('SituacionJuridicaId')->nullable();
            $table->foreign('SituacionJuridicaId')->references('SituacionJuridicaId')->on('CatSituacionJuridica');

            $table->unsignedBigInteger('TipoOrdenId')->nullable();
            $table->foreign('TipoOrdenId')->references('TipoOrdenId')->on('CatTipoOrden');

            $table->unsignedBigInteger('SentenciaId')->nullable();
            $table->foreign('SentenciaId')->references('SentenciaId')->on('Sentencias');
            $table->unsignedBigInteger('RecursoId')->nullable();
            $table->foreign('RecursoId')->references('RecursoId')->on('Recursos');
            $table->unsignedBigInteger('OdaId')->nullable();
            $table->foreign('OdaId')->references('OdaId')->on('Oda');

           
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('DelitosCausaPenal');
    }
}
