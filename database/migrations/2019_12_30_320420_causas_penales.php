<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CausasPenales extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('CausasPenales', function (Blueprint $table) {
            $table->bigIncrements('CausaPenalId');
            $table->string('CausaPenalIdGenerado', 20)->unique();
            $table->integer('CausaPenalFolio');
            $table->integer('CausaPenalAnio');
            $table->longText('CausaPenalAveriguacionPrevia');
            $table->longText('CausaPenalPedimentoPenal');
            $table->longText('CausaPenalDescripcion');
            $table->enum('CausaPenalDetenido', ['SI', 'NO']);
            $table->enum('CausaPenalEstatus', ['VIGENTE', 'ELIMINADA']);
            $table->timestamps();

            // references
             $table->unsignedBigInteger('UsuarioId');
             $table->foreign('UsuarioId')->references('UsuarioId')->on('Usuarios');

             $table->unsignedBigInteger('JuzgadoId');
             $table->foreign('JuzgadoId')->references('JuzgadoId')->on('Juzgados');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('CausasPenales');
    }
}
