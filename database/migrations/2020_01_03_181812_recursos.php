<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Recursos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Recursos', function (Blueprint $table) {
            $table->bigIncrements('RecursoId');

            $table->longText('RecursoOfrecimientoPruebas')->nullable();
            $table->longText('RecursoCierreAudiencia')->nullable();
            $table->longText('RecursoApelacion')->nullable();
            $table->longText('RecursoExtensionPenal')->nullable();
            $table->longText('RecursoSobreseimiento')->nullable();
            $table->longText('RecursoObservacion')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Recursos');
    }
}
