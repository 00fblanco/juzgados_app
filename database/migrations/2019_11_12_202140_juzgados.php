<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class Juzgados extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Juzgados', function (Blueprint $table) {
            $table->bigIncrements('JuzgadoId');
            $table->string('JuzgadoDescripcion', 50)->unique();
            $table->string('JuzgadoDomicilio', 50);
            $table->string('JuzgadoTelefono', 50);
            $table->timestamps();

             // references
             $table->unsignedBigInteger('TipoJuzgadoId');
             $table->foreign('TipoJuzgadoId')->references('TipoJuzgadoId')->on('TiposJuzgado');

             $table->unsignedBigInteger('RegionId');
             $table->foreign('RegionId')->references('RegionId')->on('Regiones');

             $table->unsignedBigInteger('DistritoId');
             $table->foreign('DistritoId')->references('DistritoId')->on('Distritos');

            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('Juzgados');
    }
}
