<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;
class AgregaSuperUsuario extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Roles')->insert(
            [
               'RolDescripcion' => 'Jefe de los Juzgados'     
            ]
        );
        DB::table('Roles')->insert(
            [
               'RolDescripcion' => 'Empleado de los Juzgados'     
            ]
        );
        DB::table('Roles')->insert(
            [
               'RolDescripcion' => 'Control de procesos'     
            ]
        );
        /* DB::table('TiposUsuario')->insert(
            [
               'TipoUsuarioDescripcion' => 'Jefe de area'     
            ]
        );
        DB::table('TiposUsuario')->insert(
            [
               'TipoUsuarioDescripcion' => 'Empleado general'     
            ]
        ); */
        // DEPARTAMENTOS
        DB::table('Departamentos')->insert(
            [
               'DepartamentoNombre' => 'Control de procesos'     
            ]
        );
        DB::table('Departamentos')->insert(
            [
               'DepartamentoNombre' => 'Adscrito a los juzgados'     
            ]
        );

        //REGIONES
        DB::table('Regiones')->insert(
            [
                ['RegionNombre' =>'CENTRO', 'created_at' => Carbon::now()] ,
                ['RegionNombre' =>'ACAPULCO', 'created_at' => Carbon::now()] ,
                ['RegionNombre' =>'NORTE', 'created_at' => Carbon::now()] ,
                ['RegionNombre' =>'TIERRA CALIENTE', 'created_at' => Carbon::now()] ,
                ['RegionNombre' =>'COSTA CHICA', 'created_at' => Carbon::now()] ,
                ['RegionNombre' =>'COSTA GRANDE', 'created_at' => Carbon::now()]   
            ]
        );
        // DISTRITOS
        DB::table('Distritos')->insert(
            [
                ['DistritoNombre' =>'GUERRERO', 'created_at' => Carbon::now()] ,
                ['DistritoNombre' =>'LOS BRAVO', 'created_at' => Carbon::now()] ,
                ['DistritoNombre' =>'TABARES', 'created_at' => Carbon::now()] ,
                ['DistritoNombre' =>'ALVAREZ', 'created_at' => Carbon::now()] ,
                ['DistritoNombre' =>'ALARCON', 'created_at' => Carbon::now()] ,
                ['DistritoNombre' =>'HIDALGO', 'created_at' => Carbon::now()] ,
                ['DistritoNombre' =>'ALDAMA', 'created_at' => Carbon::now()] ,
                ['DistritoNombre' =>'MORELOS', 'created_at' => Carbon::now()] ,
                ['DistritoNombre' =>'ZARAGOZA', 'created_at' => Carbon::now()] ,
                ['DistritoNombre' =>'MONTAÑA', 'created_at' => Carbon::now()] ,
                ['DistritoNombre' =>'MINA', 'created_at' => Carbon::now()] ,
                ['DistritoNombre' =>'CUAUHTEMOC', 'created_at' => Carbon::now()] ,
                ['DistritoNombre' =>'AZUETA', 'created_at' => Carbon::now()] ,
                ['DistritoNombre' =>'GALEANA', 'created_at' => Carbon::now()] ,
                ['DistritoNombre' =>'MONTES DE OCA', 'created_at' => Carbon::now()] ,
                ['DistritoNombre' =>'ABASOLO', 'created_at' => Carbon::now()] ,
                ['DistritoNombre' =>'ALTAMIRANO', 'created_at' => Carbon::now()] ,
                ['DistritoNombre' =>'ALLENDE', 'created_at' => Carbon::now()] 
            ]
        );

        // TIPO JUZGADO
        DB::table('TiposJuzgado')->insert(
            [
                ['TipoJuzgadoDescripcion' =>'JUZGADO PRIMERO', 'created_at' => Carbon::now()] ,
                ['TipoJuzgadoDescripcion' =>'JUZGADO SEGUNDO', 'created_at' => Carbon::now()] ,
                ['TipoJuzgadoDescripcion' =>'JUZGADO TERCERO', 'created_at' => Carbon::now()] ,
                ['TipoJuzgadoDescripcion' =>'JUZGADO CUARTO', 'created_at' => Carbon::now()] ,   
            ]
        );

        // CREA JUZGADO
        DB::table('Juzgados')->insert([
            ['JuzgadoDescripcion' =>' JUZGADO SEPTIMO DE DISTRITO '  , 'JuzgadoDomicilio' => 'Conocido',
            'JuzgadoTelefono' => '4324234',
            'TipoJuzgadoId' => 1,
            'RegionId' => 1,
            'DistritoId' => 1, 'created_at' => Carbon::now()] ,
            ['JuzgadoDescripcion' =>' JUZGADO CUARTO DE DISTRITO '  , 'JuzgadoDomicilio' => 'Conocido',
            'JuzgadoTelefono' => '4324234',
            'TipoJuzgadoId' => 1,
            'RegionId' => 1,
            'DistritoId' => 1, 'created_at' => Carbon::now()] ,
            ['JuzgadoDescripcion' =>' JUZGADO QUINTO DE DISTRITO '  , 'JuzgadoDomicilio' => 'Conocido',
            'JuzgadoTelefono' => '4324234',
            'TipoJuzgadoId' => 1,
            'RegionId' => 1,
            'DistritoId' => 1, 'created_at' => Carbon::now()] ,
            ['JuzgadoDescripcion' =>' JUZGADO OCTAVO DE DISTRITO '  , 'JuzgadoDomicilio' => 'Conocido',
            'JuzgadoTelefono' => '4324234',
            'TipoJuzgadoId' => 1,
            'RegionId' => 1,
            'DistritoId' => 1, 'created_at' => Carbon::now()] ,
            ['JuzgadoDescripcion' =>' JUZGADO SEGUNDO DE DISTRITO '  , 'JuzgadoDomicilio' => 'Conocido',
            'JuzgadoTelefono' => '4324234',
            'TipoJuzgadoId' => 1,
            'RegionId' => 1,
            'DistritoId' => 1, 'created_at' => Carbon::now()] ,
            ['JuzgadoDescripcion' =>' JUZGADO SEXTO DE DISTRITO '  , 'JuzgadoDomicilio' => 'Conocido',
            'JuzgadoTelefono' => '4324234',
            'TipoJuzgadoId' => 1,
            'RegionId' => 1,
            'DistritoId' => 1, 'created_at' => Carbon::now()] ,
            ['JuzgadoDescripcion' =>' JUZGADO TERCERO DE DISTRITO '  , 'JuzgadoDomicilio' => 'Conocido',
            'JuzgadoTelefono' => '4324234',
            'TipoJuzgadoId' => 1,
            'RegionId' => 1,
            'DistritoId' => 1, 'created_at' => Carbon::now()] ,
            ['JuzgadoDescripcion' =>' JUZGADO NOVENO DE DISTRITO '  , 'JuzgadoDomicilio' => 'Conocido',
            'JuzgadoTelefono' => '4324234',
            'TipoJuzgadoId' => 1,
            'RegionId' => 1,
            'DistritoId' => 1, 'created_at' => Carbon::now()] ,
            ['JuzgadoDescripcion' =>' JUZGADO DECIMO DE DISTRITO '  , 'JuzgadoDomicilio' => 'Conocido',
            'JuzgadoTelefono' => '4324234',
            'TipoJuzgadoId' => 1,
            'RegionId' => 1,
            'DistritoId' => 1, 'created_at' => Carbon::now()] ,
            ['JuzgadoDescripcion' =>' SEGUNDO TRIBUNAL COLEGIADO '  , 'JuzgadoDomicilio' => 'Conocido',
            'JuzgadoTelefono' => '4324234',
            'TipoJuzgadoId' => 1,
            'RegionId' => 1,
            'DistritoId' => 1, 'created_at' => Carbon::now()] ,
            ['JuzgadoDescripcion' =>' PRIMER TRIBUNAL COLEGIADO '  , 'JuzgadoDomicilio' => 'Conocido',
            'JuzgadoTelefono' => '4324234',
            'TipoJuzgadoId' => 1,
            'RegionId' => 1,
            'DistritoId' => 1, 'created_at' => Carbon::now()] ,
            ['JuzgadoDescripcion' =>' JUZGADO PRIMERO PENAL BRAVOS '  , 'JuzgadoDomicilio' => 'Conocido',
            'JuzgadoTelefono' => '4324234',
            'TipoJuzgadoId' => 1,
            'RegionId' => 1,
            'DistritoId' => 1, 'created_at' => Carbon::now()] ,
            ['JuzgadoDescripcion' =>' JUZGADO SEGUNDO PENAL BRAVOS '  , 'JuzgadoDomicilio' => 'Conocido',
            'JuzgadoTelefono' => '4324234',
            'TipoJuzgadoId' => 1,
            'RegionId' => 1,
            'DistritoId' => 1, 'created_at' => Carbon::now()] ,
            ['JuzgadoDescripcion' =>' JUZGADO TERCERO PENAL BRAVOS '  , 'JuzgadoDomicilio' => 'Conocido',
            'JuzgadoTelefono' => '4324234',
            'TipoJuzgadoId' => 1,
            'RegionId' => 1,
            'DistritoId' => 1, 'created_at' => Carbon::now()] ,
            ['JuzgadoDescripcion' =>' SALA PRIMERA PENAL DE BRAVOS '  , 'JuzgadoDomicilio' => 'Conocido',
            'JuzgadoTelefono' => '4324234',
            'TipoJuzgadoId' => 1,
            'RegionId' => 1,
            'DistritoId' => 1, 'created_at' => Carbon::now()] 
        ]); 
         


        //$TipoUsuario = DB::table("TiposUsuario")->where('TipoUsuarioDescripcion','Jefe de area')->first();
        $Rol = DB::table("Roles")->where('RolDescripcion','Jefe de los juzgados')->first();
        $Departamento = DB::table("Departamentos")->where('DepartamentoNombre','Control de procesos')->first();
        DB::table('Usuarios')->insert([
            'UsuarioNick' => 'dennis',
            'UsuarioContrasenia' => Hash::make('123456'),
            'UsuarioNombre' => "Dennis",
            'UsuarioApPaterno' => "Analco",
            'UsuarioApMaterno' => "Leyva",
            'UsuarioApiKey' => '',
            'UsuarioEstatus' => 'Activo',
            'RolId' => $Rol -> RolId,
            'DepartamentoId' => $Departamento->DepartamentoId
        ]); 

        DB::table('CatDelitos')->insert([
            ['DelitoDescripcion' =>'Robo ', 'created_at' => Carbon::now()] ,
            ['DelitoDescripcion' =>'Abigeato ', 'created_at' => Carbon::now()] ,
            ['DelitoDescripcion' =>'Abuso de Confianza', 'created_at' => Carbon::now()] ,
            ['DelitoDescripcion' =>'Fraude', 'created_at' => Carbon::now()] ,
            ['DelitoDescripcion' =>'Administración Fraudulenta', 'created_at' => Carbon::now()] ,
            ['DelitoDescripcion' =>'Extorsión', 'created_at' => Carbon::now()] ,
            ['DelitoDescripcion' =>'Usura', 'created_at' => Carbon::now()] ,
            ['DelitoDescripcion' =>'Despojo', 'created_at' => Carbon::now()] ,
            ['DelitoDescripcion' =>'Daño a la Propiedad', 'created_at' => Carbon::now()] ,
            ['DelitoDescripcion' =>'Encubrimineto por Receptación', 'created_at' => Carbon::now()] ,
            ['DelitoDescripcion' =>'Incumplimiento de la Obligación Alimentaria', 'created_at' => Carbon::now()] ,
            ['DelitoDescripcion' =>'Sustracción de Menores', 'created_at' => Carbon::now()] ,
            ['DelitoDescripcion' =>'Tráfico de Personas Menores de Edad ', 'created_at' => Carbon::now()] ,
            ['DelitoDescripcion' =>'Delitos Contra la Filiación y el Estado Civil ', 'created_at' => Carbon::now()] ,
            ['DelitoDescripcion' =>'Bigamia', 'created_at' => Carbon::now()] ,
            ['DelitoDescripcion' =>'Incesto', 'created_at' => Carbon::now()] 

        ]);

        DB::table('CatTipoActor')->insert([
            ['TipoActorDescripcion' =>'Inculpado', 'created_at' => Carbon::now()] ,
            ['TipoActorDescripcion' =>'Agraviado', 'created_at' => Carbon::now()] 
        ]);

        DB::table('CatSentencias')->insert([
            ['SentenciaDescripcion' =>'ABSOLUTORIA', 'created_at' => Carbon::now()] ,
            ['SentenciaDescripcion' =>'CONDENATORIA', 'created_at' => Carbon::now()] ,
            ['SentenciaDescripcion' =>'INTERLOCUTORIA', 'created_at' => Carbon::now()] 
        ]);

        DB::table('CatTipoOrden')->insert([
            ['TipoOrdenDescripcion' =>'Ninguna', 'created_at' => Carbon::now()] ,
            ['TipoOrdenDescripcion' =>'Orden De Aprehensión', 'created_at' => Carbon::now()] ,
            ['TipoOrdenDescripcion' =>'Orden De Comparecencia', 'created_at' => Carbon::now()] ,
            ['TipoOrdenDescripcion' =>'Orden De Reaprehensión', 'created_at' => Carbon::now()] ,
            ['TipoOrdenDescripcion' =>'Accion Pública', 'created_at' => Carbon::now()] 
        ]);

        DB::table('CatSituacionJuridica')->insert([
            ['SituacionJuridicaDescripcion' =>'PROCESADO  EN PRISIÓN', 'created_at' => Carbon::now()] ,
            ['SituacionJuridicaDescripcion' =>'SENTENCIADO  EN PRISIÓN', 'created_at' => Carbon::now()] ,
            ['SituacionJuridicaDescripcion' =>'PROCESADO LIBRE CON AMPARO', 'created_at' => Carbon::now()] ,
            ['SituacionJuridicaDescripcion' =>'PROCESADO LIBRE CON CAUCIÓN', 'created_at' => Carbon::now()] ,
            ['SituacionJuridicaDescripcion' =>'SENTENCIADO CON CAUCIÓN', 'created_at' => Carbon::now()] ,
            ['SituacionJuridicaDescripcion' =>'EVADIDO', 'created_at' => Carbon::now()] ,
            ['SituacionJuridicaDescripcion' =>'SENTENCIADO EVADIDO', 'created_at' => Carbon::now()] ,
            ['SituacionJuridicaDescripcion' =>'REMITIDO AL CONSEJO TUTELAR', 'created_at' => Carbon::now()] 
        ]);
    }
}
