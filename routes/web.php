<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});


$router->post('login/','UsuarioController@authenticate');



$router->group(['middleware' => 'auth'], function () use ($router) {
    $router->get('/usuarios','UsuarioController@listAll');
    $router->get('/usuario/{id}','UsuarioController@getById');
    $router->post('/usuarios','UsuarioController@store');
    $router->put('/usuarios','UsuarioController@update');
    $router->post('/usuario_estatus','UsuarioController@setEstatus');
    
    $router->get('/catalogos/deptos_roles', 'CatalogosController@getDeptosRoles');
    $router->get('/catalogos/juzgados', 'CatalogosController@getJuzgados');
    $router->get('/catalogos/tipos_actor', 'CatalogosController@getTiposActor');
    $router->get('/catalogos/cat_delitos', 'CatalogosController@getCatDelitos');
    $router->get('/catalogos/cat_situacion_juridica', 'CatalogosController@getCatSituacionJuridica');
    $router->get('/catalogos/cat_tipo_orden', 'CatalogosController@getCatTipoOrden');
    $router->get('/catalogos/cat_sentencias', 'CatalogosController@getCatSentencias');

    $router->put('/delito','CausasPenalesController@updateDelito');
    $router->put('/delito/add_sentencia','CausasPenalesController@addSentencia');
    $router->put('/delito/update_sentencia','CausasPenalesController@updateSentencia');
    $router->put('/delito/update_recurso','CausasPenalesController@updateRecurso');

    $router->put('/delito/add_recurso','CausasPenalesController@addRecurso');
    $router->put('/delito/add_oda','CausasPenalesController@addOda');
    $router->put('/delito/update_oda','CausasPenalesController@updateOda');

    $router->post('/causa_penal/add_actor', 'CausasPenalesController@addActor');
    $router->put('/causa_penal/update_actor', 'CausasPenalesController@updateActor');
    $router->post('/causa_penal/add_agraviado', 'CausasPenalesController@addAgraviado');
    $router->post('/causas_penales', 'CausasPenalesController@store');
    $router->get('/causas_penales', 'CausasPenalesController@getCausasPenales');
    $router->get('/causas_penales_r1/{year}/{month}', 'CausasPenalesController@getCausasPenalesR1');
    $router->get('/causas_penales_delitos/{id}', 'CausasPenalesController@getDelitosDeCausaPenal');
    $router->get('/causas_penales_agraviados/{id}', 'CausasPenalesController@getAgraviados');
    $router->get('/causas_penales/{id}','CausasPenalesController@getById');
    $router->put('/causas_penales','CausasPenalesController@update');
    $router->put('/causas_penales_estatus','CausasPenalesController@setEstatus');
});

